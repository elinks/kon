#ifndef EL__ECMASCRIPT_DELAYED_OPEN_H
#define EL__ECMASCRIPT_DELAYED_OPEN_H

struct document_view;

/** Used by delayed_open() and delayed_goto_uri_frame(). */
struct delayed_open {
	struct session *ses;
	struct uri *uri;
	unsigned char *target;
};

void delayed_open(void *);
void delayed_goto_uri_frame(void *);

void location_goto(struct document_view *doc_view, unsigned char *url);

#endif
